#!/usr/bin/env bash

picom --experimental-backends &
#~/.fehbg &
nm-applet &
nitrogen --restore &
xrandr --output HDMI-1-1 --mode 1920x1080 --output eDP-1 --mode 1920x1080 --left-of HDMI-1-1
